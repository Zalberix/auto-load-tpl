<?php 

	// возвращает false если нет совпадений и возвращает массив если они есть
	function regexGroupPathTpl ($tpl_name) {
		// Соответсвует шаблону path/to/file , все '../', './', '.' , 'file.exe' игнорируются 
		// в строке path/../../to/file.php/<>/?sa/?s./files найдет path, to, files

		$regex_paht_tpl = '/(?<=\/|^)([^_][a-zA-Z_]+)(?=\/|$)/m'; 	

		preg_match_all($regex_paht_tpl, $tpl_name, $elements_path_tpl, PREG_PATTERN_ORDER, 0);

		$groups_match = $elements_path_tpl[1];

		if (count($groups_match) == 0) return false;

		return $groups_match;

	}
// Валидация параметров
	if (count($_REQUEST) == 0) {
		echo json_encode(["error" => "not parameres"]);
		return 0;
	}

	if (!isset($_REQUEST["tpl_name"])) {
		echo json_encode(["error" => "not param tpl name"]);
		return 0;
	}

	if (!isset($_REQUEST["date"])) {
		echo json_encode(["error" => "not param date"]);
		return 0;
	}

	if (!is_numeric(strtotime($_REQUEST["date"]))) {
		echo json_encode(["error" => "invalid date"]);
		return 0;
	}

	$regex_group_paht_tpl = regexGroupPathTpl($_REQUEST["tpl_name"]);

	if (!$regex_group_paht_tpl) {
		echo json_encode(["error" => "invalid tpl name"]);
		return 0;
	}
// Валидация параметров

	$count_groups_match = count($regex_group_paht_tpl)-1;

	$tpl_name = "";

	for($i = 0; $i < $count_groups_match; $i++ ){
		$group_match = $regex_group_paht_tpl[$i];
		$path_tpl .= $group_match.'/';
	}

	$tpl_name .= $regex_group_paht_tpl[$count_groups_match];

	$path_tpl = __DIR__."/tpl/".$tpl_name. ".template.php" ;


	if(!is_readable($path_tpl)){
		echo json_encode(["path_tpl"=>$path_tpl,"error" => "this file does not exist"]);
		return 0;
	}

 ?>