<?php ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js" integrity="sha512-RNLkV3d+aLtfcpEyFG8jRbnWHxUqVZozacROI4J2F1sTaDqo1dPQYs01OMi1t1w9Y2FdbSCDSQ2ZVdAC8bzgAg==" crossorigin="anonymous"></script>
	<title>auto-load-tpl</title>
</head>
<body>
	<div>
		<button class="js-load-tpl" data-tpl-name="checkList">Загрузить шаблон 1</button>
		<button class="js-load-tpl" data-tpl-name="meny">Загрузить шаблон 2</button>
		<button class="js-clear-storage-tpl">Очистить хранилище</button>
	</div>

	<script>
		function getNow (){
			let dateObj = new Date();
			let date = dateObj.toLocaleDateString();
			let time = dateObj.toLocaleTimeString();

			return date+' '+time;
		}
		window.onload = function () {
			$(".js-load-tpl").on("click", function () {
				let dataTime = getNow();
				let dataAttributs = $(this).data();
				let tpl_name = dataAttributs["tplName"];

				let tpl_storage = sessionStorage.getItem(tpl_name);
				if(!!tpl_storage){
					console.log("шаблон есть в хранилище");

					$.ajax({
					  	url: "chechDate.php",
					  	data: {"date":dataTime, tpl_name}
					})
					.done(function (json){
						json_parse = JSON.parse(json);
						sessionStorage.setItem(json_parse["tpl_name"], json );
					})


				}else {
					console.log("получение шаблона");

					$.ajax({
					  	url: "getTemplate.php",
					  	data: {tpl_name}
					})
					.done(function (json){
						let json_parse = JSON.parse(json);
						if('error' in json_parse){
							console.log(json_parse);
						}else{
							sessionStorage.setItem(json_parse["tpl_name"], json );
						}
					})

				}
			});

			$(".js-clear-storage-tpl").on("click", function (){
				sessionStorage.clear();
			});
		}
	</script>
</body>
</html>